<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Laizhandou\HyCrontabPro;

use Hyperf\Contract\StdoutLoggerInterface;
use Laizhandou\HyCrontabPro\Configure\ConfigureInterface;

class CrontabManager
{
    /**
     * @var Crontab[]
     */
    protected $crontabs = [];

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var ConfigureInterface
     */
    private $configure;

    /**
     * @var StdoutLoggerInterface
     */
    private $logger;

    public function __construct(Parser $parser, ConfigureInterface $container, StdoutLoggerInterface $logger)
    {
        $this->parser = $parser;
        $this->configure = $container;
        $this->logger = $logger;
    }

    public function register(Crontab $crontab): bool
    {
        if (! $this->isValidCrontab($crontab) || ! $crontab->isEnable()) {
            return false;
        }
        $this->crontabs[$crontab->getName()] = $crontab;
        return true;
    }

    /**
     * 载入定时任务配置
     */
    public function loading(): array
    {
        $crontabs = $this->configure->parseCrontabs(); //获取配置

        $logout = [];
        $allName = [];
        foreach ($crontabs as $crontab) {
            if ($crontab instanceof Crontab) {
                $allName[] = $crontab->getName();
                $status = $this->registerV2($crontab);
                if ($status) {
                    $status != 1 && $logout[] = $crontab->getName();
                    $this->logger->debug(sprintf('Crontab [%s] have been registered. status: %s', $crontab->getName(), $status));
                }
            }
        }
        if ($this->configure::IS_TOTAL_GAS == 1) {
            $atName = array_keys($this->crontabs);
            $diff = array_diff($atName, $allName);
            foreach ($diff as $name) {
                $logout[] = $name;
                unset($this->crontabs[$name]);
            }
        }
        return $logout;
    }

    /**
     * @param Crontab $crontab
     * @return int 0校验不通过 1注册 2注销 3变更
     */
    public function registerV2(Crontab $crontab): int
    {
        if (! $this->isValidCrontab($crontab)) {
            return 0;
        }
        if (! $crontab->isEnable() || ! $this->indate($crontab)) { //关闭定时任务
            if (isset($this->crontabs[$crontab->getName()])) {
                unset($this->crontabs[$crontab->getName()]);
                return 2;
            } else {
                return 0;
            }
        }
        if (isset($this->crontabs[$crontab->getName()])) { //校验规则是否变更
            if ($crontab->getRule() == $this->crontabs[$crontab->getName()]->getRule()
                && $crontab->getCallback() == $this->crontabs[$crontab->getName()]->getCallback()
            ) {
                return 0;
            }
            $this->crontabs[$crontab->getName()] = $crontab;
            return 3;
        }
        $this->crontabs[$crontab->getName()] = $crontab;
        return 1;
    }


    public function parse(): array
    {
        $result = [];
        $crontabs = $this->getCrontabs();
        $lastTime = time();
        $lastTimeZero = $lastTime - date('s', $lastTime);
        foreach ($crontabs ?? [] as $key => $crontab) {
            if (! $crontab instanceof Crontab) {
                unset($this->crontabs[$key]);
                continue;
            }
            $parseTime = $crontab->getNextParseTime();
            if ($parseTime > $lastTime) {
                continue;
            }
            if ($parseTime + 60 <= $lastTime) {
                $time = $this->parser->parse($crontab->getRule(), $lastTimeZero);
                if ($time) {
                    foreach ($time as $t) {
                        if ($t->timestamp > $lastTime) {
                            $result[] = clone $crontab->setExecuteTime($t);
                        }
                    }
                }
            }
            $time = $this->parser->parse($crontab->getRule(),  $lastTimeZero + 60);
            if ($time) {
                foreach ($time as $t) {
                    $result[] = clone $crontab->setExecuteTime($t);
                }
            }
            $this->crontabs[$key]->setNextParseTime($lastTimeZero + 60);
        }
        return $result;
    }

    public function getCrontabs(): array
    {
        return $this->crontabs;
    }

    private function indate(Crontab $crontab): bool
    {
        $time = time();
        if ($crontab->getStartTime() && $crontab->getStartTime() > $time) {
            return false;
        }
        if ($crontab->getEndTime() && $crontab->getEndTime() < $time) {
            return false;
        }
        return true;
    }

    private function isValidCrontab(Crontab $crontab): bool
    {
        return $crontab->getName() && $crontab->getRule() && $crontab->getCallback() && $this->parser->isValid($crontab->getRule());
    }
}
