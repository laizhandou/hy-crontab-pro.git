<?php


namespace Laizhandou\HyCrontabPro;


class CrontabLogout
{
    /**
     * @var null|array
     */
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

}