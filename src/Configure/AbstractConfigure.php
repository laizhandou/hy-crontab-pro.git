<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Laizhandou\HyCrontabPro\Configure;

use Laizhandou\HyCrontabPro\Crontab;

abstract class AbstractConfigure implements ConfigureInterface
{
    public const IS_TOTAL_GAS = 1; //1返回全量数据 2返回变更数据

    public function buildCrontabByConfig($cfg)
    {
        $crontab = new Crontab();
        isset($cfg['name']) && $crontab->setName($cfg['name']);
        isset($cfg['type']) && $crontab->setType($cfg['type']);
        isset($cfg['rule']) && $crontab->setRule($cfg['rule']);
        isset($cfg['singleton']) && $crontab->setSingleton($cfg['singleton']);
        isset($cfg['mutexPool']) && $crontab->setMutexPool($cfg['mutexPool']);
        isset($cfg['mutexExpires']) && $crontab->setMutexExpires($cfg['mutexExpires']);
        isset($cfg['oneServer']) && $crontab->setOnOneServer($cfg['oneServer']);
        isset($cfg['callback']) && $crontab->setCallback($cfg['callback']);
        isset($cfg['memo']) && $crontab->setMemo($cfg['memo']);
        isset($cfg['enable']) && $crontab->setEnable($cfg['enable']);
        isset($cfg['startTime']) && $crontab->setStartTime($cfg['startTime']);
        isset($cfg['endTime']) && $crontab->setEndTime($cfg['endTime']);
        return $crontab;
    }

    public function isValidConfig($cfg)
    {
        if (!isset($cfg['name']) || !isset($cfg['rule']) || !isset($cfg['callback'])) {
            return false;
        }
        return true;
    }
}
