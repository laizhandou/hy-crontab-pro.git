<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Laizhandou\HyCrontabPro\Configure;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Utils\ApplicationContext;
use Laizhandou\HyCrontabPro\Annotation\Crontab as CrontabAnnotation;
use Laizhandou\HyCrontabPro\Crontab;
use Hyperf\Di\Annotation\AnnotationCollector;

class ConfigConfigure extends AbstractConfigure
{
    public const IS_TOTAL_GAS = 1;

    /**
     * @var \Hyperf\Contract\StdoutLoggerInterface
     */
    protected $logger;

    /**
     * @var \Hyperf\Contract\ConfigInterface
     */
    private $config;

    public function __construct(StdoutLoggerInterface $logger, ConfigInterface $config)
    {
        $this->logger = $logger;
        $this->config = $config;
    }

    public function parseCrontabs(): array
    {
        $configCrontabs = $this->config->get($this->config->get('crontab.configName'), []);
        if ($configCrontabs && is_string($configCrontabs)) {
            $configCrontabs = json_decode($configCrontabs, true);
        }
        $annotationCrontabs = AnnotationCollector::getClassesByAnnotation(CrontabAnnotation::class);
        $methodCrontabs = $this->getCrontabsFromMethod();
        $crontabs = [];
        foreach (array_merge($configCrontabs, $annotationCrontabs, $methodCrontabs) as $crontab) {
            if ($crontab instanceof CrontabAnnotation) {
                $crontab = $this->buildCrontabByAnnotation($crontab);
            }
            if ($crontab instanceof Crontab) {
                $crontabs[$crontab->getName()] = $crontab;
            }
            if (is_array($crontab) && $this->isValidConfig($crontab)) {
                $crontabs[$crontab['name']] = $this->buildCrontabByConfig($crontab);
            }
        }
        return array_values($crontabs);
    }

    private function getCrontabsFromMethod(): array
    {
        $result = AnnotationCollector::getMethodsByAnnotation(CrontabAnnotation::class);
        $crontabs = [];
        foreach ($result as $item) {
            $crontabs[] = $item['annotation'];
        }
        return $crontabs;
    }

    private function buildCrontabByAnnotation(CrontabAnnotation $annotation): Crontab
    {
        $crontab = new Crontab();
        isset($annotation->name) && $crontab->setName($annotation->name);
        isset($annotation->type) && $crontab->setType($annotation->type);
        isset($annotation->rule) && $crontab->setRule($annotation->rule);
        isset($annotation->singleton) && $crontab->setSingleton($annotation->singleton);
        isset($annotation->mutexPool) && $crontab->setMutexPool($annotation->mutexPool);
        isset($annotation->mutexExpires) && $crontab->setMutexExpires($annotation->mutexExpires);
        isset($annotation->onOneServer) && $crontab->setOnOneServer($annotation->onOneServer);
        isset($annotation->callback) && $crontab->setCallback($annotation->callback);
        isset($annotation->memo) && $crontab->setMemo($annotation->memo);
        isset($annotation->enable) && $crontab->setEnable($this->resolveCrontabEnableMethod($annotation->enable));

        return $crontab;
    }

    /**
     * @param array|bool $enable
     */
    private function resolveCrontabEnableMethod($enable): bool
    {
        if (is_bool($enable)) {
            return $enable;
        }

        $className = reset($enable);
        $method = end($enable);

        try {
            $reflectionClass = ReflectionManager::reflectClass($className);
            $reflectionMethod = $reflectionClass->getMethod($method);

            if ($reflectionMethod->isPublic()) {
                if ($reflectionMethod->isStatic()) {
                    return $className::$method();
                }

                $container = ApplicationContext::getContainer();
                if ($container->has($className)) {
                    return $container->get($className)->{$method}();
                }
            }

            $this->logger->info('Crontab enable method is not public, skip register.');
        } catch (\ReflectionException $e) {
            $this->logger->error('Resolve crontab enable failed, skip register.');
        }

        return false;
    }
}
